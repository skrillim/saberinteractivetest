﻿using System.Text.RegularExpressions;

namespace SaberInteractiveTest.Models
{
    /// <summary>
    /// Двусвязный список.
    /// </summary>
    public class ListRandom
    {
        #region Public Fields

        /// <summary>
        /// Кол-во элементов.
        /// </summary>
        public int Count;

        /// <summary>
        /// Начальный элемент.
        /// </summary>
        public ListNode Head;

        /// <summary>
        /// Конечный элемент.
        /// </summary>
        public ListNode Tail;

        /// <summary>
        /// Добавляет новый объект.
        /// </summary>
        /// <param name="newNode">Новый объект.</param>
        /// <param name="randomNodeIndex">Индекс случайного элемента.</param>
        public void Add(ListNode newNode, int? randomNodeIndex = null)
        {
            if (Head == null)
            {
                Head = newNode;
            }
            else
            {
                Tail.Next = newNode;
                newNode.Previous = Tail;
            }

            Tail = newNode;
            Count++;

            //Инициализация ListNode.Random.
            if (randomNodeIndex == null)
            {
                newNode.Random = GetRandomElement();
            }
            else
            {
                newNode.Random = ElementAt(randomNodeIndex.Value);
            }
        }

        /// <summary>
        /// Очищает список.
        /// </summary>
        public void Clear()
        {
            Head = null;
            Tail = null;
            Count = 0;
        }

        /// <summary>
        /// Возвращает элемент списка по его индексу.
        /// </summary>
        /// <param name="index">Индекс элемента.</param>
        /// <returns>Элемент списка.</returns>
        private ListNode ElementAt(int index)
        {
            ListNode searchedNode = null;
            ListNode currentNode;

            //Поиск элемента с начала или с конца списка в зависимости от индекса.
            if (index > Count / 2)
            {
                currentNode = Tail;

                for (int i = 0; i < Count - index; i++)
                {
                    searchedNode = currentNode;
                    currentNode = currentNode.Previous;
                }
            }
            else
            {
                currentNode = Head;

                for (int i = 0; i < index + 1; i++)
                {
                    searchedNode = currentNode;
                    currentNode = currentNode.Next;
                }
            }

            return searchedNode;
        }

        /// <summary>
        /// Возвращает рандомный элемент.
        /// </summary>
        /// <returns>Рандомный элемент.</returns>
        private ListNode GetRandomElement()
        {
            Random random = new Random();

            int randomIndex = random.Next(0, Count);

            return ElementAt(randomIndex);
        }

        /// <summary>
        /// Возвращает индекс элемента.
        /// </summary>
        /// <param name="searchedNode">Элемент, индекс которого необходимо найти.</param>
        /// <returns>Индекс элемента в списке. Если на находит, возвращает -1.</returns>
        private int IndexOf(ListNode searchedNode)
        {
            var count = 0;

            if (searchedNode != null)
            {
                for (ListNode node = Head; node != null; node = node.Next, count++)
                {
                    if (searchedNode.Equals(node))
                    {
                        return count;
                    }
                }
            }

            return -1;
        }

        #endregion Public Fields

        #region Public Methods

        /// <summary>
        /// Преабразует поток байтов в данный объект.
        /// </summary>
        /// <param name="stream">Поток для чтения.</param>
        public void Deserialize(Stream stream)
        {
            Clear();

            using (StreamReader streamReader = new StreamReader(stream))
            {
                string? currentline;
                string searchPattern = string.Format("({0}|{1})=\"(.+?)\"", nameof(ListNode.Data), nameof(ListNode.Random));

                while ((currentline = streamReader.ReadLine()) != null)
                {
                    ListNode newNode = new ListNode();

                    MatchCollection matches = Regex.Matches(currentline, searchPattern);

                    if (matches.Count != 0)
                    {
                        string nodeData = matches[0].Groups[2].Value;
                        string nodeRandom = matches[1].Groups[2].Value;

                        //Десериализация escape-кодов.
                        string unescapedData = Regex.Unescape(nodeData);

                        newNode.Data = unescapedData;
                        int randomElementIndex = Convert.ToInt32(nodeRandom);

                        Add(newNode, randomElementIndex);
                    }
                }
            }
        }

        /// <summary>
        /// Записывает данный объект в поток байтов.
        /// </summary>
        /// <param name="stream">Поток для записи.</param>
        public void Serialize(Stream stream)
        {
            ListNode currentNode = Head;

            using (StreamWriter streamWriter = new StreamWriter(stream))
            {
                string newLineTemplate = string.Format("<{0} {1}=\"{2}\" {3}=\"{4}\" />", nameof(ListNode), nameof(ListNode.Data), "{0}", nameof(ListNode.Random), "{1}");

                while (true)
                {
                    int randomElementIndex = IndexOf(currentNode.Random);

                    string newLine = string.Format(newLineTemplate, currentNode.Data, randomElementIndex);

                    //Cериализация escape-кодов.
                    string escapedNewLine = Regex.Escape(newLine);

                    streamWriter.WriteLine(escapedNewLine);

                    if (currentNode.Next == null)
                    {
                        break;
                    }

                    currentNode = currentNode.Next;
                }
            }
        }

        #endregion Public Methods
    }
}