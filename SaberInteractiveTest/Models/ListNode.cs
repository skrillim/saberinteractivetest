﻿namespace SaberInteractiveTest.Models
{
    /// <summary>
    /// Элемент двусвязного списка.
    /// </summary>
    public class ListNode
    {
        #region Public Fields

        /// <summary>
        /// Значение элемента.
        /// </summary>
        public string Data;

        /// <summary>
        /// Следующий элемент.
        /// </summary>
        public ListNode Next;

        /// <summary>
        /// Предыдущий элемент.
        /// </summary>
        public ListNode Previous;

        /// <summary>
        /// Произвольный элемент.
        /// </summary>
        public ListNode Random;

        #endregion Public Fields
    }
}