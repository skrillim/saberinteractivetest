﻿using SaberInteractiveTest.Models;

public static class Program
{
    #region Public Methods

    /// <summary>
    /// Файл для сериализации.
    /// </summary>
    private const string SERIALIZATION_FILE_NAME = @"SerializatedListRandom.xml";

    /// <summary>
    /// Список элементов.
    /// </summary>
    private static ListRandom _nodeList;

    public static void Main()
    {
        _nodeList = new ListRandom();

        SetData();

        //Сериализация.
        using (Stream writeStream = new FileStream(SERIALIZATION_FILE_NAME,
                FileMode.Create, FileAccess.Write))
        {
            _nodeList.Serialize(writeStream);
        }

        //Десериализация.
        using (Stream readStream = new FileStream(SERIALIZATION_FILE_NAME,
            FileMode.Open, FileAccess.Read))
        {
            _nodeList.Deserialize(readStream);
        }
    }

    /// <summary>
    /// Заполняет <see cref="_nodeList"/> данными.
    /// </summary>
    private static void SetData()
    {
        Random random = new Random();

        for (int i = 0; i < 10; i++)
        {
            ListNode newNode = new ListNode()
            {
                Data = random.Next(0, 100).ToString()
            };

            _nodeList.Add(newNode);
        }
    }

    #endregion Public Methods
}